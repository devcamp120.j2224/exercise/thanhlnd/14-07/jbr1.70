public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(23,59,59);
        Time time2 = new Time(14,0,0);
        System.out.println("Thời gian 1:");
        time1.display(); 
        System.out.println("Thời gian 2:");
        time2.display(); 
        System.out.println("Thời gian 1 sau khi tăng 1 giây:");
        time1.nextSecond();
        time1.display();
        System.out.println("Thời gian 2 sau khi giảm 1 giây:");
        time2.previousSecond();
        time2.display();
    }
}
